//
//  PaymentViewController.swift
//  Chirpcoin
//
//  Created by Samuel Laska on 3/13/16.
//  Copyright © 2016 Samuel Laska. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController {
    
    var parent:ViewController?

    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var declineButton: UIButton!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        confirmButton.layer.cornerRadius = 5
        confirmButton.layer.borderWidth = 1
        confirmButton.layer.borderColor = confirmButton.currentTitleColor.CGColor
        
        declineButton.layer.cornerRadius = 5
        declineButton.layer.borderWidth = 1
        declineButton.layer.borderColor = declineButton.currentTitleColor.CGColor
        
        if let d = parent?.dictionary, address = d["address"] as? String, name = d["name"] as? String, amount = d["amount"] as? NSString {
            nameLabel.text = name
            addressLabel.text = address
            amountLabel.text = "\(amount.floatValue) BTC"
            
            let amount = (amount as NSString).floatValue
            
            if parent!.balance - amount < 0 {
                confirmButton.enabled = false
                confirmButton.alpha = 0.3
            } else {
                confirmButton.enabled = true
                confirmButton.alpha = 1.0
            }
        }
    }
    @IBAction func confirmTapped(sender: AnyObject) {
        parent?.executeLastTransaction()
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func declineTapped(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    

}
