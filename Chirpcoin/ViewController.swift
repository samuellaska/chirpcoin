//
//  ViewController.swift
//  Chirpcoin
//
//  Created by Samuel Laska on 3/12/16.
//  Copyright © 2016 Samuel Laska. All rights reserved.
//

import UIKit

class ViewController: UIViewController, PPKControllerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var ppk:PPKController?
    let chirp = ChirpSDK.sdk()
    let defaults = NSUserDefaults.standardUserDefaults()
    
    var loggedIn = false
    var accessToken = ""
    var refreshToken = ""
    var dictionary:[NSObject:AnyObject]?
    var client:Coinbase?
    var primaryAccount:CoinbaseAccount?
    let overlayTransitioningDelegate = OverlayTransitioningDelegate()
    var bitcoinAddress = ""
    var balance:Float = 0.0
    
    var txs = [["Martin Losak", "0.001 BTC"],["John Doe", "0.002 BTC"]]
    
    @IBOutlet weak var requestButton: UIButton!
    @IBOutlet weak var keyboardView: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var chirpWaveForm: ChirpWaveformView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var signInButton: UIBarButtonItem!
    @IBOutlet weak var textField: UITextField!
    
    let p2pKitKey = "eyJzaWduYXR1cmUiOiIzRXJMNkIvaE42bTRBTHpHSFJKdWt5ODRBQUI5S25RK3E5UU92aHU2ZVlIMkQyRnVmSEpiTkFsNlZvdSszRHZCNnE5MjIySjFXd3lhajZqUVhpOXZhdWtpYmFNM3poZFZtcENqSERHTXhXNlRPQzlBYW1BR2pXNHVOeW5rSERaL3l5cC8yOXk3bnpjbjg2ZVV5cm5MczdEMXJGemtoRjl3OVNaRnZtTFZ5aTg9IiwiYXBwSWQiOjE0OTAsInZhbGlkVW50aWwiOjE2OTYyLCJhcHBVVVVJRCI6IjI3NERFNDU0LUU0NDAtNEU2MS1COEE4LTZEQjI5N0MwQkQwOCJ9"
    let chirpKey = "dvd019NCpaUCWn8TeIH96lX6B"
    let chirpSecret = "rPUMGtsPsn6ucH0qnLB0Bh33u3pVi3Xj36zWYxZzeo99lIsSXB"
    let scope = "balance user addresses send"
    let coinbaseClientId = "5b86f05a70970ac9932055ae65b4cfdd63c25b6a52a53d67a31420153378374a"
    let coinbaseSecret = "e1718fdaad7f937aca3a4aac8b72d922584eb215b272bded603b39605550dd8f"
    
    let returnAddress = "1CrgTPwJ57ZNTE9XU51q2ErxUUt6JYwcuu"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        txs.removeAll()
        
        // Chirp
        chirp.setAppKey(chirpKey, andSecret: chirpSecret) { (authenticated, error) in
            print("chirp: \(authenticated) \(error)")
        }
        chirp.startListening { (chirp, error) in
            print("received: \(chirp.shortcode)")
            print("data: \(chirp.data)")
            self.dictionary = chirp.data
            self.performSegueWithIdentifier("showPayment", sender: self)
        }
        chirpWaveForm.audioBuffer = chirp.audioBuffer
        
        // CoinBase
        if let token = defaults.stringForKey("access_token") {
            accessToken = token
            loggedIn = true
            client = Coinbase(OAuthAccessToken: token)
        }
        
        // TableView
        tableView.delegate = self
        tableView.dataSource = self
        
        requestButton.layer.cornerRadius = requestButton.frame.width/2        
        
        keyboardView.hidden = true
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "authenticationComplete:", name: "authorize", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name:UIKeyboardWillHideNotification, object: nil);
    }
    
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        updateUI()
    }
    
    // MARK: - CoinBase
    
    @IBAction func signInTapped(sender: AnyObject) {
        if loggedIn {
            loggedIn = false
            client = nil
            accessToken = ""
            defaults.removeObjectForKey("access_token")
            updateUI()
        } else {
            CoinbaseOAuth.startOAuthAuthenticationWithClientId(coinbaseClientId, scope: scope, redirectUri: "chirpcoin://coinbase-oauth", meta: ["send_limit_amount":"10","send_limit_currency":"USD","send_limit_period":"day"])
        }
        updateUI()
    }
    
    
    func authenticationComplete(notification: NSNotification) {
        if let response = notification.userInfo!["result"] as? NSDictionary {
            if let token = response.objectForKey("access_token") as? String {
                accessToken = token
                defaults.setObject(accessToken, forKey: "access_token")
                defaults.synchronize()
                client = Coinbase(OAuthAccessToken: self.accessToken)
                loggedIn = true
            }
            
            if let token = response.objectForKey("refresh_token") as? String {
                refreshToken = token
            }
        }
        
        updateUI()
    }
    
    func refreshTokens() {
        print("refreshing tokens")
        CoinbaseOAuth.getOAuthTokensForRefreshToken(refreshToken, clientId: coinbaseClientId, clientSecret: coinbaseSecret) { (response, error) in
            if error != nil {
                print("could not refresh tokens \(error)")
            } else {
                print("Got new tokens, loading email")
                if let token = response.objectForKey("refresh_token") as? String {
                    self.refreshToken = token
                }
                if let token = response.objectForKey("access_token") as? String {
                    self.client = Coinbase(OAuthAccessToken: token)
                    self.client?.getCurrentUser({ (user, error) in
                        if error != nil {
                            print("could not load user: \(user)")
                        } else {
                            print("user email: \(user.email)")
                        }
                    })
                }
            }
        }
    }
    
    func updateUI() {
        if loggedIn {
            client?.getAccountsList({ (accounts, paging, error) in
                if error != nil {
                    print ("could not load accounts \(error)")
                } else {
                    for a in accounts as! [CoinbaseAccount] {
                        if a.primary == true {
                            self.balance = (a.balance.amount as NSString).floatValue
                            self.balanceLabel.text = "\(self.balance) BTC"
                            
                            self.primaryAccount = CoinbaseAccount(ID: a.accountID, client: self.client)
                            dispatch_async(dispatch_get_main_queue(), {
                                self.getBitcoinAddress()
                            })
                        }
                    }
                }
            })
            client?.getCurrentUser({ (user, error) in
                if error != nil {
                    print("could not load user: \(user)")
                } else {
                    print("user email: \(user.email) \(user.name)")
                    self.userNameLabel.text = user.name
                }
            })
            signInButton.title = "Sign Out"
        } else {
            signInButton.title = "Sign In"
        }
    }
    
    func getBitcoinAddress() {
        primaryAccount?.getBitcoinAddress { (address, error) in
            print("address: \(address.address)")
            self.bitcoinAddress = address.address
        }
    }
    
    func executeLastTransaction() {
        if let d = dictionary, address = d["address"] as? String, name = d["name"] as? String, amount = d["amount"] as? String {
            primaryAccount?.sendAmount(amount, to: address, completion: { (transaction, error) in
                self.txs.append([name, amount])
                self.tableView.reloadData()
                self.updateUI()
                print("transaction executed: \(transaction) \(error)")
            })
        }
    }
    
    // debug
//    @IBAction func sendMoney(sender: AnyObject) {
//        primaryAccount?.sendAmount("0.0001", to: self.returnAddress, completion: { (transaction, error) in
//            print("transaction id: \(transaction.transactionID) hsh: \(transaction.hshString) hash: \(transaction.hashString)")
//            // https://blockchain.info/tx/6a74ac968f0c0f50b337e7df67708312e5c66230168f3b296ec30ee92d63fc7d?show_adv=true
//        })
//    }
    
    
    // MARK: - Chirp
    
    @IBAction func startChirping() {
        textField.becomeFirstResponder()
    }
    
    // MARK: - p2pKit
//    func PPKControllerInitialized() {
//        PPKController.startP2PDiscoveryWithDiscoveryInfo(nil)
//    }
    
    // MARK: - TableView
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return txs.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("txCell", forIndexPath: indexPath)
        cell.textLabel?.text = txs[indexPath.row][0]
        cell.detailTextLabel?.text = "-\(txs[indexPath.row][1])"
        return cell
    }
    
    
    // MARK: - Actions
    
    @IBAction func requestTapped(sender: AnyObject) {
        textField.resignFirstResponder()
        let data = ["address":bitcoinAddress, "amount":textField.text!, "name":userNameLabel.text!]
        chirp.createChirpWithDictionary(data) { (chirp, error) in
            print("sent: \(chirp.shortcode) \(error)")
            chirp.chirp()
        }
        textField.text = ""
    }
    
    func keyboardWillShow(notification: NSNotification) {
        var info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        keyboardView.hidden = false
        
        self.bottomConstraint.constant = keyboardFrame.size.height
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            self.keyboardView.layoutIfNeeded()
        })
    }
    
    func keyboardWillHide(notification: NSNotification) {
        keyboardView.hidden = true
        self.bottomConstraint.constant = 0
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            self.keyboardView.layoutIfNeeded()
        })
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "showPayment", let overlayVC = segue.destinationViewController as? PaymentViewController {
            overlayVC.parent = self
            overlayVC.transitioningDelegate = overlayTransitioningDelegate
            overlayVC.modalPresentationStyle = .Custom
        }
    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
}

